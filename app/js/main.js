function sectionCentered() {
    if ($('.section').hasClass('section--centered')) {
        var vh = $(window).outerHeight();
        var siteHeaderHeight = $('.site-header').outerHeight();
        var mainPadding = parseFloat($('.main').css('padding-top'));
        var tableOffset = $('.section--centered').offset().top;
        $('.section--centered').outerHeight(vh - (tableOffset));
    }
};

$(document).ready(function() {
    var navlistItem = $('.site-navlist li');
    var siteAsideMobileBtn = $('.site-aside__mobile-nav');
    var siteAsideBackBtn = $('.site-aside__backbtn');
    var portfolioItem = $('.main-portfolio__list li');

    navlistItem.hover(function() {
        if (!$(this).hasClass('current-page')) {
            $(this).siblings('.current-page').addClass('faded');
        }
    }, function() {
        $(this).siblings('.current-page').removeClass('faded');
    });

    siteAsideBackBtn.on('click', function(event) {
        event.preventDefault();
        history.go(-1);
    });

    // mobileBtn
    siteAsideMobileBtn.on('click', function(event) {
        event.preventDefault();
        $(this).parents('.site-aside').find('.site-navlist').toggleClass('site-navlist--active');
    });
    // END:mobileBtn

    // inner-portfolio
    portfolioItem.hover(function() {
        $(this).siblings().addClass('faded');
    }, function() {
        $(this).siblings().removeClass('faded');
    });
    // END:inner-portfolio

    sectionCentered();
    $(window).resize(function(event) {
        sectionCentered();
    });
});











