var app = angular.module("form", []);
	
app.directive("bookingForm", function(){
	return {
		restrict: "A",
		controllerAs: "boForm",
		controller: function(){
			this.checkboxAmount = 0;
			this.formData = {};

			this.checkboxCheck = function(checked){

				if(checked)
					this.checkboxAmount++;
				else
					this.checkboxAmount--;
				
				console.log(checked);
				console.log(this.checkboxAmount);
			}
			
			this.send = function(){
				
				if(this.checkboxAmount > 0)
					$.ajax({
						url: "/ajax/getOrder.php",
						type: "POST",
						data: $("form[name = 'order']").serialize(),
						success: function(data){
							alert(data);
						}
					});
				else
					alert("Выберите тип сайта");
			}
		},
	};
});
