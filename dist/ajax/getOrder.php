<?php
require_once './phpMailer.php'; 

$pmailer = new PHPMailer;
$pmailer->Subject = 'Сайт';

$pmailer->CharSet = 'UTF-8';
$pmailer->AddAddress('mygvozdev@gmail.com', 'Александр Гвоздев');


$body = '';

if(isset($_POST['vizitka'])
	|| isset($_POST['land'])
	|| isset($_POST['portal'])
	|| isset($_POST['shop'])
	){
		$body .= 'Типы сайта:';
		
		if(!empty($_POST['vizitka']))
			$body .= ' Сайт-визитка'."\r\n";

		if(!empty($_POST['land']))
			$body .= ' Landing page'."\r\n";

		if(!empty($_POST['portal']))
			$body .= ' Портал'."\r\n";

		if(!empty($_POST['shop']))
			$body .= ' Интернет-магазин'."\r\n";
	}

if(!empty($_POST['concept']))
	$body .= 'Концепция: ' . $_POST['concept']."\r\n";

if(!empty($_POST['audience']))
	$body .= 'Аудитория: ' . $_POST['audience']."\r\n";

if(!empty($_POST['target']))
	$body .= 'Цель: ' . $_POST['target']."\r\n";

if(!empty($_POST['goals']))
	$body .= 'Задачи сайта: ' . $_POST['goals']."\r\n";

if(!empty($_POST['likeWebSites1']))
	$body .= 'Понравившийся сайт: ' . $_POST['likeWebSites1']."\r\n";

if(!empty($_POST['likeWebSites2']))
	$body .= 'Понравившийся сайт: ' . $_POST['likeWebSites2']."\r\n";

if(!empty($_POST['likeWebSites3']))
	$body .= 'Понравившийся сайт: ' . $_POST['likeWebSites3']."\r\n";

if(!empty($_POST['contact_name']))
	$body .= 'Имя: ' . $_POST['contact_name']."\r\n";

if(!empty($_POST['contact_phone']))
	$body .= 'Телефон: ' . $_POST['contact_phone']."\r\n";

if(!empty($_POST['contact_email']))
	$body .= 'E-mail: ' . $_POST['contact_email']."\r\n";

$pmailer->Body = $body;
echo '“Спасибо! Мы свяжемся с вами в ближайшее время”';
$pmailer->send();

